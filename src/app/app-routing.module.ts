import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  {
    path: 'dashboard',
    loadChildren: 'app/features/+dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'detail/:id',
    loadChildren: 'app/features/+hero-detail/hero-detail.module#HeroDetailModule'
  },
  {
    path: 'heroes',
    loadChildren: 'app/features/+hero-list/hero-list.module#HeroListModule'
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
