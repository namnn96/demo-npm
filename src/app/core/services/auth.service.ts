import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { take } from 'rxjs/operators';

import { LoggerService } from './logger.service';

@Injectable({ providedIn: 'root' })
export class AuthService {

  private accessToken: string;
  private tokenPayload: any;

  constructor(
    private http: HttpClient,
    private _loggerSvc: LoggerService
  ) {
    this.accessToken = null;
  }

  login(username, password): void {
    const url = '';
    this.http.post(url, { username, password })
      .pipe(take(1))
      .subscribe((result: any) => {
        this._loggerSvc.log(result);
        this.setAccessToken(result.accessToken);
      });
  }

  logout(): void {
    this.accessToken = null;
    this.tokenPayload = null;
    localStorage.removeItem('access_token');
  }

  isAuthenticated(): boolean {
    return !!this.accessToken;
  }

  isExpired(): boolean {
    return this.tokenPayload == null || (this.tokenPayload.exp * 1000) < Date.now();
  }

  getAccessToken(): string {
    return this.accessToken;
  }

  setAccessToken(accessToken: string): void {
    this.accessToken = null;
    this.tokenPayload = null;

    if (typeof (accessToken) !== 'string') {
      return;
    }

    if (!!accessToken) {
      // Remove spaces and carriage returns
      accessToken = accessToken.replace('/\s/g', '');

      // Extract the payload from the token
      const [, payloadStr, ] = accessToken.split('.');
      const decodedPayload = atob(payloadStr);
      const payload: any = typeof (decodedPayload) === 'string' ? JSON.parse(decodedPayload) : decodedPayload;

      // Check if token is missing payload or is expired
      if (!payload || Date.now() >= payload.exp * 1000) {
        return;
      }

      localStorage.setItem('access_token', this.accessToken);
      this.accessToken = accessToken;
      this.tokenPayload = payload;
    }
  }

  getTokenPayload() {
    return this.tokenPayload;
  }
}
