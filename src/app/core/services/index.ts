import { AuthService } from './auth.service';
import { InMemoryDataService } from './in-memory-data.service';
import { LoggerService } from './logger.service';

export * from './auth.service';
export * from './in-memory-data.service';
export * from './logger.service';

export const AppServices = [
  AuthService,
  InMemoryDataService,
  LoggerService
];
