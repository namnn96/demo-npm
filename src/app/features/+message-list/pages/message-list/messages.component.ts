import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { MessageService } from '../../services';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  public messages$: Observable<string[]>;

  constructor(public _messageSvc: MessageService) {}

  ngOnInit() {
    this.messages$ = this._messageSvc.messages$;
  }

  clear() {
    this._messageSvc.clear();
  }
}
