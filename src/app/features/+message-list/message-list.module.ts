import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MessageListRoutingModule } from './message-list-routing.module';
import { messageListServices } from './services';
import { messageListPages } from './pages';

@NgModule({
  declarations: [...messageListPages],
  exports: [...messageListPages],
  imports: [
    CommonModule,
    MessageListRoutingModule
  ],
  providers: [messageListServices]
})
export class MessageListModule {}
