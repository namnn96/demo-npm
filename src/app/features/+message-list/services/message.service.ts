import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class MessageService {
  private _messages: string[] = [];
  public messages$: BehaviorSubject<string[]> = new BehaviorSubject([]);

  add(message: string) {
    this._messages.push(message);
    this.messages$.next(this._messages);
  }

  clear() {
    this._messages = [];
    this.messages$.next(this._messages);
  }
}
