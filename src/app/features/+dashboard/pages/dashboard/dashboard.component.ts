import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { Hero } from '../../../+hero-list/models';
import { select, Store } from '@ngrx/store';
import { selectAllHeroes } from '../../../+hero-list/store/selectors';
import { LoadHeroListStart } from '../../../+hero-list/store/actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  public heroes$: Observable<Hero[]>;
  public topHeroes$: Observable<Hero[]>;

  constructor(private store: Store<any>) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.store.dispatch(new LoadHeroListStart(null));
    this.heroes$ = this.store.pipe(select(selectAllHeroes));
    this.topHeroes$ = this.heroes$.pipe(
      map((heroes: Hero[]) => heroes.slice(1, 5))
    );
  }
}
