import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { dashboardPages } from './pages';
import { HeroListComponentModule } from '../+hero-list/hero-list-component.module';

@NgModule({
  declarations: [...dashboardPages],
  exports: [...dashboardPages],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    HeroListComponentModule
  ],
  providers: []
})
export class DashboardModule {}
