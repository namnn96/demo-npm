import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { HeroDetailRoutingModule } from './hero-detail-routing.module';
import { heroDetailPages } from './pages';

@NgModule({
  declarations: [...heroDetailPages],
  exports: [...heroDetailPages],
  imports: [
    CommonModule,
    FormsModule,
    HeroDetailRoutingModule
  ],
  providers: []
})
export class HeroDetailModule {}
