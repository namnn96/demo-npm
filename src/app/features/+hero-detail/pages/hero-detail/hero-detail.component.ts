import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { take } from 'rxjs/operators';

import { Hero } from '../../../+hero-list/models';
import { HeroService } from '../../../+hero-list/services';
import { UpdateHeroStart } from '../../../+hero-list/store/actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: [ './hero-detail.component.css' ]
})
export class HeroDetailComponent implements OnInit {
  @Input() hero: Hero;

  constructor(
    private route: ActivatedRoute,
    private _heroSvc: HeroService,
    private location: Location,
    private store: Store<any>
  ) {}

  ngOnInit(): void {
    this.getHero();
  }

  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this._heroSvc.getHero(id).pipe(take(1))
      .subscribe(hero => this.hero = hero);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.store.dispatch(new UpdateHeroStart(this.hero));
  }
}
