import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HeroDetailComponent } from './pages';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: HeroDetailComponent }
    ])
  ],
  exports: [RouterModule]
})
export class HeroDetailRoutingModule { }
