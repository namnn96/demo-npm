import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Hero } from '../../models';
import { select, Store } from '@ngrx/store';
import { selectAllHeroes } from '../../store/selectors';
import { AddHeroStart, DeleteHeroStart, LoadHeroListStart } from '../../store/actions';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  public heroes$: Observable<Hero[]>;

  constructor(private store: Store<any>, private router: Router) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.store.dispatch(new LoadHeroListStart(null));
    this.heroes$ = this.store.pipe(select(selectAllHeroes));
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.store.dispatch(new AddHeroStart({ name } as Hero));
  }

  delete(hero: Hero): void {
    this.store.dispatch(new DeleteHeroStart(hero));
  }

  async viewHero(event, id: string) {
    event.preventDefault();
    await this.router.navigate(['detail', id]);
  }
}
