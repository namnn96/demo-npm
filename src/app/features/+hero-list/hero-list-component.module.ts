import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { heroListComponents } from './components';

@NgModule({
  declarations: [...heroListComponents],
  exports: [...heroListComponents],
  imports: [
    CommonModule
  ],
  providers: []
})
export class HeroListComponentModule {}
