import { createSelector } from '@ngrx/store';
import * as fromHeroList from '../reducers/hero-list.reducer';
import { selectHeroListState } from '../reducers';

export const selectAllHeroes = fromHeroList.adapter.getSelectors(selectHeroListState).selectAll;

export const selectHeroListLoading = createSelector(
  selectHeroListState,
  (state: fromHeroList.State) => state.loading
);

export const selectHeroListError = createSelector(
  selectHeroListState,
  (state: fromHeroList.State) => state.error
);
