import * as fromHeroList from './hero-list.reducer';
import { createFeatureSelector } from '@ngrx/store';

export type State = fromHeroList.State;

export const initialState: State = fromHeroList.initialState;

export const reducers = fromHeroList.reducer;

export const selectHeroListState = createFeatureSelector<State>('hero-list');
