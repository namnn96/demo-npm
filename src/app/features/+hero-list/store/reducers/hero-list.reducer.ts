import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { Hero } from '../../models';
import {
  ADD_HERO_FAILURE, ADD_HERO_START,
  DELETE_HERO_FAILURE, DELETE_HERO_START,
  HeroListAction,
  LOAD_HERO_LIST_FAILURE,
  LOAD_HERO_LIST_START,
  LOAD_HERO_LIST_SUCCESS, UPDATE_HERO_FAILURE, UPDATE_HERO_START
} from '../actions';

export interface State extends EntityState<Hero> {
  loading: boolean;
  error: any;
}

export function selectId(hero: Hero): number {
  return hero.id;
}

export const adapter: EntityAdapter<Hero> = createEntityAdapter<Hero>({
  selectId: selectId,
  sortComparer: false
});

export const initialState: State = adapter.getInitialState({
  loading: false,
  error: null
});

export function reducer(state: State = initialState, action: HeroListAction): State {
  let nextState = state;
  switch (action.type) {
    case LOAD_HERO_LIST_START:
    case ADD_HERO_START:
    case DELETE_HERO_START:
    case UPDATE_HERO_START: {
      nextState = { ...state, loading: true };
      break;
    }
    case LOAD_HERO_LIST_SUCCESS: {
      nextState = {
        ...adapter.upsertMany(action.payload || [], state),
        loading: false
      };
      break;
    }
    case LOAD_HERO_LIST_FAILURE:
    case ADD_HERO_FAILURE:
    case DELETE_HERO_FAILURE:
    case UPDATE_HERO_FAILURE: {
      nextState = { ...state, error: action.payload, loading: false };
      break;
    }

    default:
      break;
  }

  return nextState;
}
