import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';
import {
  ADD_HERO_START, AddHeroFailure,
  AddHeroStart, DELETE_HERO_START, DeleteHeroFailure, DeleteHeroStart,
  LOAD_HERO_LIST_START,
  LoadHeroListFailure,
  LoadHeroListStart,
  LoadHeroListSuccess, UPDATE_HERO_START, UpdateHeroFailure, UpdateHeroStart
} from '../actions';
import { Hero } from '../../models';
import { HeroService } from '../../services';
import { Location } from '@angular/common';

@Injectable()
export class HeroListEffect {
  @Effect()
  loadHeroListStart$ = this.actions$.pipe(
    ofType<LoadHeroListStart>(LOAD_HERO_LIST_START),
    exhaustMap(() => {
      return this._heroListSvc.getHeroes().pipe(
        map((heroList: Hero[]) => new LoadHeroListSuccess(heroList)),
        catchError(error => of(new LoadHeroListFailure(error)))
      );
    })
  );

  @Effect()
  addHeroStart$ = this.actions$.pipe(
    ofType<AddHeroStart>(ADD_HERO_START),
    map(action => action.payload),
    exhaustMap((hero) => {
      return this._heroListSvc.addHero(hero).pipe(
        map((heroList: Hero[]) => new LoadHeroListSuccess(heroList)),
        catchError(error => of(new AddHeroFailure(error)))
      );
    })
  );
  @Effect()
  deleteHeroStart$ = this.actions$.pipe(
    ofType<DeleteHeroStart>(DELETE_HERO_START),
    map(action => action.payload),
    exhaustMap((hero) => {
      return this._heroListSvc.deleteHero(hero).pipe(
        map((heroList: Hero[]) => new LoadHeroListSuccess(heroList)),
        catchError(error => of(new DeleteHeroFailure(error)))
      );
    })
  );
  @Effect()
  updateHeroStart$ = this.actions$.pipe(
    ofType<UpdateHeroStart>(UPDATE_HERO_START),
    map(action => action.payload),
    exhaustMap((hero) => {
      return this._heroListSvc.updateHero(hero).pipe(
        tap(() => this.location.back()),
        map((heroList: Hero[]) => new LoadHeroListSuccess(heroList)),
        catchError(error => of(new UpdateHeroFailure(error)))
      );
    })
  );

  constructor(
    private actions$: Actions,
    private _store: Store<any>,
    private _heroListSvc: HeroService,
    private location: Location
  ) {}
}
