import { Action } from '@ngrx/store';
import { Hero } from '../../models';

export const LOAD_HERO_LIST_START = '[Hero list] load start';
export const LOAD_HERO_LIST_SUCCESS = '[Hero list] load success';
export const LOAD_HERO_LIST_FAILURE = '[Hero list] load failure';

export const ADD_HERO_START = '[Hero] add start';
export const ADD_HERO_FAILURE = '[Hero] add failure';
export const DELETE_HERO_START = '[Hero] delete start';
export const DELETE_HERO_FAILURE = '[Hero] delete failure';
export const UPDATE_HERO_START = '[Hero] update start';
export const UPDATE_HERO_FAILURE = '[Hero] update failure';

export class LoadHeroListStart implements Action {
  readonly type = LOAD_HERO_LIST_START;
  constructor(public payload: any) {}
}
export class LoadHeroListSuccess implements Action {
  readonly type = LOAD_HERO_LIST_SUCCESS;
  constructor(public payload: Hero[]) {}
}
export class LoadHeroListFailure implements Action {
  readonly type = LOAD_HERO_LIST_FAILURE;
  constructor(public payload: any) {}
}

export class AddHeroStart implements Action {
  readonly type = ADD_HERO_START;
  constructor(public payload: Hero) {}
}
export class AddHeroFailure implements Action {
  readonly type = ADD_HERO_FAILURE;
  constructor(public payload: any) {}
}

export class DeleteHeroStart implements Action {
  readonly type = DELETE_HERO_START;
  constructor(public payload: Hero | number) {}
}
export class DeleteHeroFailure implements Action {
  readonly type = DELETE_HERO_FAILURE;
  constructor(public payload: any) {}
}

export class UpdateHeroStart implements Action {
  readonly type = UPDATE_HERO_START;
  constructor(public payload: Hero) {}
}
export class UpdateHeroFailure implements Action {
  readonly type = UPDATE_HERO_FAILURE;
  constructor(public payload: any) {}
}

export type HeroListAction =
  LoadHeroListStart
  | LoadHeroListSuccess
  | LoadHeroListFailure
  | AddHeroStart
  | AddHeroFailure
  | DeleteHeroStart
  | DeleteHeroFailure
  | UpdateHeroStart
  | UpdateHeroFailure;
