import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { HeroListEffect, reducers } from './store';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('hero-list', reducers),
    EffectsModule.forFeature([HeroListEffect])
  ],
  providers: []
})
export class HeroListStateModule {}
