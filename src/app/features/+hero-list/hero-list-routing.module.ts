import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HeroesComponent } from './pages';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: HeroesComponent }
    ])
  ],
  exports: [RouterModule]
})
export class HeroListRoutingModule { }
