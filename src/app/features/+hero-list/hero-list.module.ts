import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { heroListPages } from './pages';
import { HeroListRoutingModule } from './hero-list-routing.module';
import { HeroListComponentModule } from './hero-list-component.module';

@NgModule({
  declarations: [...heroListPages],
  exports: [...heroListPages],
  imports: [
    CommonModule,
    HeroListComponentModule,
    HeroListRoutingModule
  ],
  providers: []
})
export class HeroListModule {}
